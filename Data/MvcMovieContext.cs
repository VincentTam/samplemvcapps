using Microsoft.EntityFrameworkCore;
using SampleMVCApps.Models;

namespace SampleMVCApps.Data
{
    public class MvcMovieContext : DbContext
    {
        public MvcMovieContext(DbContextOptions<MvcMovieContext> options)
            : base(options)
        {
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
            => optionsBuilder
                .UseNpgsql("Host=localhost;Port=5432;Database=MvcMovie;Username=mydev;Password=my0eV;Integrated Security=true;Pooling=true")
                .UseSnakeCaseNamingConvention();
        public DbSet<Movie> Movies { get; set; }
    }
}
